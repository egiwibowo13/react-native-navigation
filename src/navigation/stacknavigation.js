import React, { Component } from "react";
import { StackNavigator } from "react-navigation";

import TabNavigation from "./tabnavigation.js";
import Login from "../screen/login/index.js";

export default StackNavigator({
    Login: {
        screen: Login,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    TabNavigation: {
        screen: TabNavigation,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    }
},{
    initialRouteName:  'Login',
});

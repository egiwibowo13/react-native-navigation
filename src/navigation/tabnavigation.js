import React, { Component } from "react";
import {
    Animated,
    TouchableWithoutFeedback,
    StyleSheet,
    View,
    Platform,
    Keyboard,
    Text
} from 'react-native';
import { StackNavigator, TabNavigator, TabBarBottom } from "react-navigation";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Beranda from "../screen/beranda/index";
import Transaksi from "../screen/transaksi/index";
import Favorite from "../screen/favorite/index";
import QR from "../screen/qr/index";
import Lainnya from "../screen/lainnya/index";


export default TabNavigator(
    {
        Beranda: {
            screen: Beranda,
            header: null,
        },
        Transaksi: {
            screen: Transaksi,
            header: null,
        },
        Favorite: {
            screen: Favorite,
            header: null,
        },
        QR: {
            screen: QR,
            header: null,
        },
        Lainnya: {
            screen: Lainnya,
            header: null, 
        }
    },
    {
        headerMode: 'none',
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === "Beranda") {
                    iconName = focused ? 'home' : 'home-outline'
                } else if (routeName === "Transaksi") {
                    iconName =  'wallet'
                } else if (routeName === "Favorite") {
                    iconName = focused ? 'heart' : 'heart-outline'
                }else if (routeName === "QR") {
                    iconName = 'qrcode'
                }else if (routeName === "Lainnya") {
                    iconName = focused ? 'dots-horizontal-circle' : 'dots-horizontal'
                }
                return (<Icon name={iconName} size={25} color={tintColor} />)
            }
        }),
        tabBarOptions: {
            activeTintColor: 'tomato',
            inactiveTintColor: 'grey'
        },
        tabBarComponent: TabBarBottom,
        tabBarPosition: "bottom",
        animationEnabled: false,
        swipeEnabled: false
    }
);
